import React, { useState } from 'react'

const NR_QUESTIONS = 10

const QUESTIONS = Array(NR_QUESTIONS)
  .fill(null)
  .map((_, idx) => ({
    idx,
    question: `Question #${ idx }`,
    answer: `Answer #${ idx }`,
    used: false,
  }))

let getFirstQuestion = () => {
  const firstQuestion = getRandomQuestion()
  getFirstQuestion = () => firstQuestion
  return firstQuestion
}

export const App = () => {
  const [{ idx, questions }, setState] = useState({ idx: 0, questions: [getFirstQuestion()] })

  const { question, answer } = questions[idx]

  const previousQuestion = () => {
    setState({
      idx: idx - 1,
      questions,
    })
  }

  const nextQuestion = () => {
    setState({
      idx: idx + 1,
      questions: idx === questions.length - 1
        ? questions.concat(getRandomQuestion())
        : questions,
    })
  }

  return <>
    <h1>{ question }</h1>
    <h2>{ answer }</h2>

    <p>{ questions.map(q => q.idx).join(', ') }</p>

    { idx > 0 && <button onClick={ previousQuestion }>Previous Question</button> }
    { idx < NR_QUESTIONS - 1 && <button onClick={ nextQuestion }>Next Question</button> }
  </>
}

function getRandomQuestion() {
  const eligible = QUESTIONS.filter(({ used }) => !used)
  const question = eligible[Math.floor(Math.random() * eligible.length)]
  question.used = true
  return question
}
